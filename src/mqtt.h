#ifndef _MQTT_H
#define _MQTT_H

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <PubSubClient.h>

extern String mqttBase;
extern PubSubClient mqttClient;

extern void init_mqtt();
extern void reconnect_mqtt();
extern void mqtt_handle();
extern void mqtt_sendPowerState(bool state);

#endif
