#include <Arduino.h>

#include <WiFiManager.h>

#include "global.h"
#include "wifi.h"


void cb_enterWifiConfigMode(WiFiManager *wifiManager)
{
	strip->setSingleColor(BLACK);
	strip->setSingleColor(BLUE, 0);
	strip->show();
}

void init_wifi()
{
	serial.println("Init WiFi");
	// WiFiManagerParameter custom_mqttServer("server", "mqtt server", mqttServer, 40);
	// WiFiManagerParameter custom_mqttPort("port", "mqtt_port", mqttPort, 6);

	WiFiManager wifiManager;
	wifiManager.setAPCallback(cb_enterWifiConfigMode);

	strip->setSingleColor(RED, 0);
	strip->show();
	//wifiManager.resetSettings();
	if(!wifiManager.autoConnect("SchnakeGhost")) {
		serial.println("Could not connect or configured");
		delay(3000);
		ESP.reset();
		delay(5000);
	}
}
