#ifndef _STRIP_H
#define _STRIP_H

#include <NeoPixelBus.h>
#include <NeoPixelAnimator.h>

#define xSTRIP_PIN 2
#define xSTRIP_PIXEL_COUNT 12

#define EFFECT_NONE 0
#define EFFECT_CIRCLE 1

#define RED RgbColor(255,0,0)
#define BLUE RgbColor(0,0,255)
#define GREEN RgbColor(0,255,0)
#define WHITE RgbColor(255,255,255)
#define BLACK RgbColor(0,0,0)

enum anim_t {
	none,
	rotate,
	cycle
};

//struct MyAnimationState {
//	RgbColor startColor;
//	RgbColor endColor;
//	AnimEaseFunction Easeing;
//};

//extern NeoGamma<NeoGammaTableMethod> colorGamma;
//extern NeoPixelBus<NeoGrbFeature, NeoEsp8266AsyncUart800KbpsMethod> strip;

//extern volatile anim_t animationType;

//extern NeoPixelAnimator animations;
//extern MyAnimationState animStates[];

class Strip
{
public:
	Strip(uint8_t pixels, uint8_t pin);
	~Strip();

	void show();

	void setSingleColor(const RgbColor &color, int8_t index = -1);
	void setSingleColor(uint8_t r, uint8_t g, uint8_t b, int8_t index = -1);
	void setSingleColor(int8_t index);
	void setBaseColor(const RgbColor &color);
	void setBaseColor(uint8_t r, uint8_t g, uint8_t b);

	void setEffect(anim_t animType);
	void setDuration(int16_t duration);
	void setFactor1(float factor1);
	void setFactor2(int16_t factor2);

	void setPower(bool power);
	bool getPower() const;

	void stopAllAnimations();
	void startEffect();

	void updateAnimations();

	bool isAnimation() const;

	uint8_t getPixels() const;

protected:
	void cbRotate(const AnimationParam &param);
	void cbCycle(const AnimationParam &param);

private:
	uint8_t m_pin;
	uint8_t m_pixels;

	bool m_power;

	NeoPixelBus<NeoGrbFeature, NeoEsp8266AsyncUart800KbpsMethod> *m_strip;
	NeoPixelAnimator *m_animation;

	NeoGamma<NeoGammaTableMethod> m_gamma;

	RgbColor m_baseColor;

	anim_t m_animType;
	int16_t m_duration;

	float m_factor1;
	int16_t m_factor2;
};

#endif
