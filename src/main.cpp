#include <Arduino.h>
#include <ArduinoJson.h>
#include <ArduinoOTA.h>

#include <math.h>

#include <NeoPixelAnimator.h>

#include <OneButton.h>

#include "global.h"
#include "ota.h"
#include "strip.h"
#include "mqtt.h"
#include "wifi.h"


OneButton button(4, true);


void mqtt_callback(char *topic, byte *payload, unsigned int length) {
	serial.printf("Topic: %s\r\n", topic);
	if(String(mqttBase +"/"+ WiFi.hostname() +"/rgb").compareTo(topic)==0) {
		char *p = new char[length+1];
		strncpy(p, (const char*)payload, length);
		p[length] = 0;
		//serial.printf("Topic match, payload: \"%s\"\n", p);

		DynamicJsonBuffer jsonBuffer(200);
		JsonObject &root = jsonBuffer.parseObject(p);

		uint8_t r, g, b;
		int8_t i = 0;
		r = g = b = 0;
		if(root.success()) {
			if(root.containsKey("i")) {
				i = root["i"];
			}
			if(root.containsKey("r")) {
				r = root["r"];
			}
			if(root.containsKey("g")) {
				g = root["g"];
			}
			if(root.containsKey("b")) {
				b = root["b"];
			}

			strip->setBaseColor(r, g, b);
			if(!strip->isAnimation()) {
				strip->setSingleColor(i);
				strip->show();
			}
		}
	} else 	if(String(mqttBase +"/"+ WiFi.hostname() +"/effect").compareTo(topic)==0) {
		char *p = new char[length+1];
		strncpy(p, (const char*)payload, length);
		p[length] = 0;

		DynamicJsonBuffer jsonBuffer(200);
		JsonObject &root = jsonBuffer.parseObject(p);

		if(root.containsKey("effect")) {
			String e = root["effect"];
			if(e == "0" || e == "none") {
				strip->setEffect(none);
			} else if(e == "1" || e == "rotate") {
				strip->setEffect(rotate);
			} else if(e == "2" || e == "cycle") {
				strip->setEffect(cycle);
			}
		}
		if(root.containsKey("duration")) {
			strip->setDuration(root["duration"]);
		}
	} else 	if(String(mqttBase +"/"+ WiFi.hostname() +"/parameter").compareTo(topic)==0) {
		char *p = new char[length+1];
		strncpy(p, (const char*)payload, length);
		p[length] = 0;

		DynamicJsonBuffer jsonBuffer(200);
		JsonObject &root = jsonBuffer.parseObject(p);

		if(root.containsKey("power")) {
			String e = root["power"];
			if(e == "on" || e == "1") {
				strip->setPower(true);
				strip->setSingleColor(-1);
				strip->show();
			} else if(e == "off" || e == "0") {
				strip->setPower(false);
				strip->setSingleColor(-1);
				strip->show();
			}
		}
		if(root.containsKey("factor1")) {
			strip->setFactor1(root["factor1"]);
		}
		if(root.containsKey("factor2")) {
			strip->setFactor2(root["factor2"]);
		}
	}
}

void buttonClicked() {
	strip->setPower(!strip->getPower());
	strip->setSingleColor(-1);
	strip->show();
}

void setup() {
    Serial.begin(115200);
    while(!Serial && !Serial.available());
	randomSeed(analogRead(0));

	//delay(1000);

	strip = new Strip(12, 2);

    serial.println("Schnakebus");
	serial.println("Ghost node base on ESP8266");
	serial.printf("Compiled at %s %s\r\n", __DATE__, __TIME__);

	init_mqtt();
	mqttClient.setCallback(mqtt_callback);

	init_wifi();
	init_ota();

	strip->setSingleColor(BLUE, 0);
	strip->show();
	delay(500);
	strip->setSingleColor(BLACK);
	strip->show();

	button.attachClick(buttonClicked);
}

void loop() {
	if(WiFi.status() == WL_CONNECTED) {
		if(!mqttClient.connected()) {
			reconnect_mqtt();
		}
		mqtt_handle();
		mqttClient.loop();
	}
	ArduinoOTA.handle();

	strip->updateAnimations();

	button.tick();
}
