#include <Arduino.h>

#include "global.h"
#include "mqtt.h"

String mqttBase("schnake/node");

char mqttServer[40] = "192.168.178.39";
char mqttPort[6] = "1883";
WiFiClient wifiClient;
PubSubClient mqttClient(wifiClient);

void init_mqtt()
{
	serial.println("Init MQTT");
	mqttClient.setServer(mqttServer, atoi(mqttPort));
}

void reconnect_mqtt()
{
	if(mqttClient.connect(WiFi.hostname().c_str())) {
		serial.printf("MQTT connected as %s\r\n", WiFi.hostname().c_str());
		mqttClient.subscribe(String(mqttBase +"/"+ WiFi.hostname() +"/rgb").c_str(), 1);
		mqttClient.subscribe(String(mqttBase +"/"+ WiFi.hostname() +"/power").c_str(), 1);
		mqttClient.subscribe(String(mqttBase +"/"+ WiFi.hostname() +"/effect").c_str(), 1);
		mqttClient.subscribe(String(mqttBase +"/"+ WiFi.hostname() +"/parameter").c_str(), 1);
	} else {
		serial.printf("MQTT faild, rc: %d\r\n", mqttClient.state());
	}
}

void mqtt_handle()
{
	mqttClient.loop();
}

void mqtt_sendPowerState(bool state) {
	String s;
	if(state) {
		s = "ON";
	} else {
		s = "OFF";
	}

	mqttClient.publish(String(mqttBase +"/"+ WiFi.hostname() +"/power/state").c_str(), s.c_str(), true);
}
