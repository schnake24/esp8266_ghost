#include <Arduino.h>
#include <ArduinoOTA.h>
#include <NeoPixelBus.h>

#include "global.h"
#include "strip.h"

void init_ota()
{
	serial.println("Init OTA");
	ArduinoOTA.begin();
	ArduinoOTA.onStart([]() {
		strip->setPower(true);
		strip->setSingleColor(BLACK);
		strip->show();
	});
	ArduinoOTA.onEnd([]() {
		strip->setSingleColor(GREEN);
		strip->show();
	});
	ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
		int leds = strip->getPixels() * progress / total;
		unsigned long int part = (strip->getPixels() * progress) % total;
		//serial.printf("OTA: [%d, %ld]\r\n", leds, part);
		for(int pixel = 0; pixel < strip->getPixels(); ++pixel) {
			if(pixel <= leds) {
				RgbColor c;
				if(pixel >= leds) {
					c = RgbColor(0, 0, part * 255L / total);
				} else {
					c = BLUE;
				}
				strip->setSingleColor(c, pixel);
			} else {
				strip->setSingleColor(BLACK, pixel);
			}
		}
		strip->show();
	});
	ArduinoOTA.onError([](ota_error_t error) {
		for(int i = 0; i < 5; ++i) {
			strip->setSingleColor(BLACK);
			strip->show();
			delay(200);
			strip->setSingleColor(RED);
			strip->show();
			delay(200);
		}
		ESP.restart();
	});
}
