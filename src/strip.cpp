#include <Arduino.h>

#include <functional>

#include "strip.h"
#include "global.h"

#include "mqtt.h"


Strip::Strip(uint8_t pixels, uint8_t pin)
{
	serial.println("Init WS2812");

	m_pin = pin;
	m_pixels = pixels;
	m_power = true;

	serial.println("Init NeoPixelBus");
	m_strip = new NeoPixelBus<NeoGrbFeature, NeoEsp8266AsyncUart800KbpsMethod>(m_pixels, m_pin);
	m_strip->Begin();
	serial.println("Init NeoPixelAnimator");
	m_animation = new NeoPixelAnimator(1);

	setFactor1(0.5);

	serial.println("Set color to BLACK");
	setSingleColor(BLACK);

	serial.println("Set effect to NONE");
	setEffect(none);

	serial.println("Ready.");
	show();
}

Strip::~Strip()
{
	delete m_animation;
	delete m_strip;
}

void Strip::setPower(bool power) {
	m_power = power;
	mqtt_sendPowerState(m_power);
}

bool Strip::getPower() const {
	return m_power;
}

uint8_t Strip::getPixels() const
{
	return m_pixels;
}

void Strip::setBaseColor(const RgbColor &color)
{
	m_baseColor = color;
}

void Strip::setBaseColor(uint8_t r, uint8_t g, uint8_t b)
{
	setBaseColor(RgbColor(r, g, b));
}

void Strip::setFactor1(float factor1)
{
	m_factor1 = factor1;
}

void Strip::setFactor2(int16_t factor2)
{
	m_factor2 = factor2;
}

void Strip::show()
{
	m_strip->Show();
}

void Strip::stopAllAnimations()
{
	serial.println("stopAllAnimations");
	m_animation->StopAnimation(0);
}


void Strip::updateAnimations()
{
	if(m_animType != none) {
		if(!m_animation->IsAnimating()) {
			startEffect();
		}
		m_animation->UpdateAnimations();
		show();
	}
}

void Strip::startEffect()
{
	if(m_power) {
		switch(m_animType) {
		case none:
			break;
		case rotate:
			//serial.println("Start ROTATE");
			m_animation->StartAnimation(0, m_duration, std::bind(&Strip::cbRotate, this, std::placeholders::_1));
			break;
		case cycle:
			m_animation->StartAnimation(0, m_duration * m_factor2, std::bind(&Strip::cbCycle, this, std::placeholders::_1));
		}
	}
}

void Strip::setDuration(int16_t duration)
{
	m_duration = duration;
}

void Strip::setEffect(anim_t animType)
{
	m_animType = animType;

	switch(m_animType) {
		case none:
			serial.println("Animation is NONE");
			stopAllAnimations();
			setSingleColor(m_baseColor);
			show();
			break;
		case rotate:
			serial.println("Animation is ROTATE");
			stopAllAnimations();
			startEffect();
			break;
		case cycle:
			break;
	}
	serial.println("Anim Effect updated");
}

void Strip::setSingleColor(const RgbColor &color, int8_t index)
{
	if(!m_power) {
		m_strip->ClearTo(BLACK);
		return;
	}
	RgbColor c = m_gamma.Correct(color);
	if(index >= 0 && index < m_pixels) {
		m_strip->SetPixelColor(index, c);
	} else {
		m_strip->ClearTo(c);
	}
}

bool Strip::isAnimation() const
{
	return(m_animType != none);
}


void Strip::setSingleColor(uint8_t r, uint8_t g, uint8_t b, int8_t index)
{
	setSingleColor(RgbColor(r, g, b), index);
}

void Strip::setSingleColor(int8_t index)
{
	setSingleColor(m_baseColor, index);
}

void Strip::cbRotate(const AnimationParam &param)
{
	float *b = new float[getPixels()*3];
	float pos = param.progress * ((float)getPixels());

	for(int x=-getPixels(); x<getPixels()*2; ++x) {
		float v = (-pow((x-pos)*m_factor1, 2))+1;
		if(v<0) v = 0.0;
		b[x+getPixels()] = v;
	}

	for(int x = 0; x < getPixels(); ++x) {
		float v = std::max(std::max(b[x], b[x+getPixels()]), b[x+getPixels()*2]);
		RgbColor c = RgbColor::LinearBlend(BLACK, m_baseColor, v);
		setSingleColor(c, x);
	}

	delete b;
}

void Strip::cbCycle(const AnimationParam &param)
{
	HsbColor color(m_baseColor);

	float h = color.H + param.progress;
	if(h > 1.0) h -= 1.0;

	HsbColor newColor(h, color.S, color.B);

	setSingleColor(RgbColor(newColor));
}
